package code;

import org.junit.Assert;
import org.junit.Test;
import section.MergeUniqueArrays;


public class MergeUniqueArraysTest {

    @Test
    public void Mergetest() {
        int [] arr1 =new int[] {1,8};
        int [] arr2 =new int[] {2,4};
        int [] z = new int [] {1,2,4,8};

        Assert.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr1, arr2), z);
    }

}
