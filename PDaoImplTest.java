package code;

import section.Patient;
import section.PatientDaoException;
import section.PatientDaoImpl;
import org.junit.Test;
import java.sql.SQLException;
import static org.junit.Assert.*;

public class PDaoImplTest {

    @Test(expected = PatientDaoException.class)
    public void ConstructorTest() throws PatientDaoException{

        PatientDaoImpl dao = new PatientDaoImpl();
    }

    @Test(expected = PatientDaoException.class)
    public void insert_patient() throws PatientDaoException {
        PatientDaoImpl dao = new PatientDaoImpl();
        Patient p = new Patient("Ahmed", "Mohamed");
        dao.insert_patient(p);

    }

}