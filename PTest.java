package code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import section.Patient;

public class PTest {
    @Test
    public void ConstructorTest() {
    	Patient p = new Patient("Ahmed","Ali");
        Assert.assertTrue(p.getfname().equals("Ahmed"));
        Assert.assertTrue(p.getlname().equals("Ali"));
    }

    @Test
    public void setSSNTest() {
        Patient p = new Patient("Ahmed","Ali");
		p.setSSN("10");
        Assert.assertTrue(p.getSSN().equals("10") );
    }

    @Test
    public void setPhNumTest() {
    	Patient p = new Patient("Ahmed","Ali");
        p.setPhNum("01000022222");
        Assert.assertTrue(p.getPhNum().equals("01000022222"));
    }

}